class LandscapeWater {
    static int calculateWaterAmount(final int[] landscape) {
        checkLandscapeLength(landscape);
        checkLandscapeHeight(landscape);

        final int[] left = calculateMaxLeftHeights(landscape);
        final int[] right = calculateMaxRightHeights(landscape);

        int sum = 0;
        // Collected water bar is a difference between minimal border pick (left or right)
        for (int i = 0; i < landscape.length; i++) {
            sum += Math.min(left[i], right[i]) - landscape[i];
        }

        return sum;
    }

    private static void checkLandscapeLength(final int[] landscape) {
        check(landscape.length <= 32000, "Landscape collection size should be less or equal than 32000");
    }

    private static void checkLandscapeHeight(final int[] landscape) {
        for (final int elem : landscape) {
            check(elem >= 0 && elem <= 32000, "Landscape height should be between 0 and 32000 including, but was " + elem);
        }
    }

    private static void check(final boolean condition, final String message) {
        if (!condition) {
            throw new RuntimeException(message);
        }
    }

    private static int[] calculateMaxLeftHeights(final int[] landscape) {
        final int[] result = new int[landscape.length];

        result[0] = landscape[0];
        for (int i = 1; i < landscape.length; i++) {
            result[i] = Math.max(result[i - 1], landscape[i]);
        }

        return result;
    }

    private static int[] calculateMaxRightHeights(final int[] landscape) {
        final int[] result = new int[landscape.length];

        result[landscape.length - 1] = landscape[landscape.length - 1];
        for (int i = landscape.length - 2; i >= 0; i--) {
            result[i] = Math.max(result[i + 1], landscape[i]);
        }

        return result;
    }
}
