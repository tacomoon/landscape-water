import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class LandscapeWaterTest {

    private static final Random random = new Random();

    @ParameterizedTest
    @MethodSource("illegal_height_landscape")
    void check_landscape_height_precondition(final int[] landscape) {
        assertThatThrownBy(() -> LandscapeWater.calculateWaterAmount(landscape))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void check_landscape_size_precondition() {
        final int[] landscape = IntStream.generate(random::nextInt)
                .limit(320001)
                .toArray();

        assertThatThrownBy(() -> LandscapeWater.calculateWaterAmount(landscape))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void check_water_amount_calculate() {
        final int[] landscape = {5, 2, 3, 4, 5, 4, 0, 3, 1};

        final int actual = LandscapeWater.calculateWaterAmount(landscape);

        assertThat(actual)
                .isEqualTo(9);
    }

    private static Stream<int[]> illegal_height_landscape() {
        return Stream.of(
                new int[]{12, 32001, 563},
                new int[]{-1, 754, 23},
                new int[]{0, -93, 32989}
        );
    }
}